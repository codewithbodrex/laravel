<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register(){
        return view('register');
    }

    public function kirim(Request $request){
        $fname = $request['first'];

        return view('welcome',['first'=>$fname]);
    }

    // public function welcome(){
    //     return view('welcome');
    // }
}
