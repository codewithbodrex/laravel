<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//facade
use RealRashid\SweetAlert\Facades\Alert;


class CastController extends Controller
{
    public function create(){
        return view('cast.tambah');
    }

    //berhubungan dengan database maka kita butuh request(post)
    public function store(Request $request){
        //validation disesuaikan dengan kebutuhan di dalam database (unique,not null,dkk)
        $request->validate([
            //namanya disamakan dengan attribut name pada page yang ditautkan
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio']
        ]);
        Alert::success('Data Added', 'Data berhasil di tambah');
        return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.tampil',['cast' => $cast]); //melempar data seperti di plain php pake"?"
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first(); //method first maksudnya ngambil data satu row
        //dd($cast);
        /*sistem fetch datanya menggunakan array associative jadi pattern pengambilannya berulang seperti ini terus*/
        return view('cast.detail' ,['cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit',['cast' => $cast]);
    }

    public function update(Request $request ,$id){
        $affected = DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request->nama,
                    'umur' => $request->umur,
                    'bio' => $request->bio
                ]
            );
        return redirect('/cast')->with('success', 'Data Berhasil dirubah');;
    }

    public function destroy($id){
        $deleted = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Data Berhasil dihapus');;
    }
}


