@extends('layout.master');

@section('judul')
    Selamat Datang Page CRUD tambah
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Cast</label>
            <input type="text" class="form-control" name="nama">
        </div>
        @error('nama') <!--Disesuaikan dengan name yang dimaksud-->
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur Cast</label>
            <input type="number" class="form-control" min="1" max="100" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror        
        <div class="form-group">
            <label>Bio Cast</label>
            <textarea type="text" class="form-control" cols="30" rows="10" name="bio"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror        
        <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection