<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
    </head>
<body>
    <form action="./kirim" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <label>First Name : </label><br><br>
        <input type="text" name="first" id="first"><br><br>
        <label>Last Name : </label><br><br>
        <input type="text" name="last" id="last"><br><br>
        <label>Gender : </label><br><br>
        <input type="radio" name="gmale" id="gmale">Male<br>
        <input type="radio" name="gmale" id="fmale">Female<br>
        <input type="radio" name="gmale" id="omale">Other<br><br>
        <label>Nationality : </label><br><br>
        <select name="nationality">
            <option value="indonesian">indonesian</option>
            <option value="foreign">Foreign</option>
        </select><br><br>
        <label>Language Spoken : </label><br><br>
        <input type="checkbox" name="gmale">Bahasa Indonesia<br>
        <input type="checkbox" name="gmale">English<br>
        <input type="checkbox" name="gmale">Other<br><br>
        <label>Bio : </label><br><br>
        <textarea name="bio"cols="30" rows="10"></textarea><br>
        <button type="submit">Sign Up</button>      

    </form>

</body>
</html>