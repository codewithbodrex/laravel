<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

// Route::get('/register', [AuthController::class, 'register']);
// Route::post('/kirim', [AuthController::class, 'kirim']);
//passing data duplicate 

// Route::get('/master', function(){
//     return view('layout.master');
// });

Route::get('/table', function(){
    return view('partial.table');
});

Route::get('/data-tables', function(){
    return view('partial.datatable');
});

//form cast
Route::get('/cast/create',[CastController::class, 'create']);

//post data ke database
Route::post('/cast',[CastController::class, 'store']);

//read file
Route::get('/cast',[CastController::class, 'index']);
//atur detail function
Route::get('/cast/{cast_id}',[CastController::class, 'show']);

//update file
Route::get('/cast/{cast_id}/edit', [CastController::class,'edit']);
Route::put('/cast/{cast_id}', [CastController::class,'update']);

//delete file
Route::delete('/cast/{cast_id}', [CastController::class,'destroy']);



